import java.util.Collections;


public class Sort {

	public static void showModels(Info info){
		int count = 1;
        System.out.println("Model sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getAuto());
        }
        Collections.sort(Ad.adList);
    }
	
	public static void showPrice(Info info){
		int count = 1;
        System.out.println("Year sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getPrice());
        }
        Collections.sort(Ad.adList);
	}
	
	public static void showMileage(Info info){
		int count = 1;
        System.out.println("Mileage sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getMileage());
        }
        Collections.sort(Ad.adList);
    }
	
	public static void showYear(Info info){
		int count = 1;
        System.out.println("Year sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getYear());
        }
        Collections.sort(Ad.adList);
	}
	
	public static void showRegion(Info info){
		int count = 1;
        System.out.println("Region sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getRegion());
        }
        Collections.sort(Ad.adList);
    }
	
	public static void showCity(Info info){
		int count = 1;
        System.out.println("City sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getCity());
        }
        Collections.sort(Ad.adList);
    }
	
	public static void showIfBroken(Info info){
		int count = 1;
        System.out.println("Is it broken sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getIfBroken());
        }
        Collections.sort(Ad.adList);
    }
	
	public static void showIfNew(Info info){
		int count = 1;
        System.out.println("Is it new sort: ");
 
        for (Info i : Ad.adList){
            System.out.println(count++ +". " + i.getIfNew());
        }
        Collections.sort(Ad.adList);
    }
	
}
