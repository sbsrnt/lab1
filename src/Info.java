public class Info implements Comparable<Info>{
	public String auto;
	public int price;
	public int mileage;
	public int year;
	public String region;
	public String city;
	public String ifBroken;
	public String ifNew;

	public String getAuto() {
		return auto;
	}


	public void setAuto(String auto) {
		this.auto = auto;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public int getMileage() {
		return mileage;
	}


	public void setMileage(int mileage) {
		this.mileage = mileage;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public String getRegion() {
		return region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getIfBroken() {
		return ifBroken;
	}


	public void setIfBroken(String ifBroken) {
		this.ifBroken = ifBroken;
	}


	public String getIfNew() {
		return ifNew;
	}


	public void setIfNew(String ifNew) {
		this.ifNew = ifNew;
	}


	public Info(String auto, int price, int mileage, int year, String region,  String city,  String ifBroken, String ifNew){
        this.auto = auto;
        this.price = price;
        this.mileage = mileage;
        this.year = year;
        this.region = region;
        this.city = city;
        this.ifBroken = ifBroken;
        this.ifNew = ifNew;   
    }
	
	public int compareTo(Info i) {
        return auto.compareTo(i.getAuto());
    }
	
}