import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Ad {
	public static List<Info> adList = new ArrayList<Info>();
	
	public static void showAd(Info info)
	{		
             System.out.println("Choosen model: " + info.getAuto() + "\n"+
            					"Price: " + info.getPrice()+ "zł \n"+
            					"Mileage: " + info.getMileage()+ "km \n"+
            					"Year: " + info.getYear()+ " \n"+
            					"City: " + info.getCity() + " \n"+
            					"Broken? " + info.getIfBroken() + "\n" +
            					"New? " + info.getIfNew() + "\n"
            		 			);
    }
	
	public static void addList(Info i)
	 {
	        adList.add(i);
	 }	
	
	public static void addAd(Info info)
	{		
		addAd(new Info("Ford", 12000, 156845, 2005,"Pomorskie", "Gdańsk", "Yes", "No"));
		addAd(new Info("Mazda", 20410, 264854, 2007, "Lubelskie", "Lublin","No", "No"));
		addAd(new Info("BMW", 38500, 68458, 2009, "Mazowieckie", "Warszawa", "No", "Yes"));
		addAd(new Info("Mercedes", 11000, 35186, 2014, "Pomorskie", "Gdynia","Yes", "Yes"));
		addAd(new Info("Skoda", 5000, 359846, 1999, "Pomorskie", "Sopot" ,"No", "No"));
		

       Collections.sort(adList);
   }
	
}
